# Internationalization Best Practices

* Application Architecture
* Visual Design
* Source Content
* Source Code

---
## Application Architecture
### Unicode
* UTF-16 - in memory
* UTF-8 - storage and transmission

### Locale-neutral data structures
* date/time - UTC
* telephone number
* person's name
* postal address
* currency value

---
## Source Content
* each string resource should contain a grammatically independent phrase
* sentences with placeholders -- watch out of grammatical agreements in the localization -- gender, plurality, etc.
* what kind of content is allowed in a string resource -- HTML tags (links, font style), low-ASCII characters only(?), ...
* images (snowman for winter, dog for searching info, body parts)
* colors

---
## Visual Design
### Localized Text

Text expansion may reveal:

* truncated text
  * does ellipses work for all languages? 
  * top and bottom of text (accents, diacritics, ligatures) may get cut off (Thai, ...) -- specific languages will need their own CSS
* overlapping text
* wrapped text incorrectly broken.

Test your design using pseudo-localization or machine translation

### Lists (properly sorted)
You should be able to get these from CLDR.

* languages
* currencies
* countries
  * states/provinces
  * prefectures (Japan) -- special sorting
* time zones

### Other Lists

* credit cards
* banks
  
### Calendar
* name of weekday (Monday, Mon, ...)
* day of month (1st, 2nd, 3rd, 4th, ...)
* month names (January, Jan)
* first day of the week (Sunday or Monday or ...?)
* Gregorian Year, ...

---   
## Source Code
Application code should be locale-neutral and locale-aware.

Application Code should not contain:

* hard-coded locale data (en_US, date formats, address formats)
* locale specific code:
  * "if (country == "en_US')"
  * name formatting (First Name, Middle Name, Last Name, ...)
  * postal address
  * telephone number
  * date formatting
  * number/currency formatting
 
 Application developers should using an i18n library instead of writing locale-aware code.
 ---
